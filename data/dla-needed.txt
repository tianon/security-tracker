An LTS security update is needed for the following source packages.
When you add a new entry, please keep the list alphabetically sorted.

The specific CVE IDs do not need to be listed, they can be gathered in an up-to-date manner from
https://security-tracker.debian.org/tracker/source-package/SOURCEPACKAGE
when working on an update.

To work on a package, simply add your name behind it. To learn more about how
this list is updated have a look at
https://wiki.debian.org/LTS/Development#Triage_new_security_issues

To make it easier to see the entire history of an update, please append notes
rather than remove/replace existing ones.

--
389-ds-base
  NOTE: 20220516: Source code is vulnerable to CVE-2022-0996. The package do not have a large install base so the
  NOTE: 20220516: priority of fixing is probably low.
--
admesh (Anton Gladky)
--
amd64-microcode
--
ansible
  NOTE: 20210411: As discussed with the maintainer I will update Buster first and
  NOTE: 20210411: after that LTS. (apo)
  NOTE: 20210426: https://people.debian.org/~apo/lts/ansible/
  NOTE: 20220427: Lee Garrett (maintainer) took over the work a while ago. See
  NOTE: 20220427: https://salsa.debian.org/debian/ansible/-/commits/stretch/
--
ark (Markus Koschany)
  NOTE: 20220424: programming language C
--
asterisk (Abhijith PA)
  NOTE: 20220424: programming language C
--
cgal
  NOTE: 20220421: many no-dsa issues, please check, whether it is possible to fix them without uploading a new upstream release (Anton)
--
ckeditor (Sylvain Beucler)
  NOTE: 20220402: multiple pendings vulnerabilities (Beuc/front-desk)
  NOTE: 20220510: no rdeps, no sponsors, most CVEs require following upstream stable 4.x,
  NOTE: 20220510: considering either ignoring, or mass-bumping all dists,
  NOTE: 20220510: waiting for ckeditor_3_ discussion to close up first (Beuc)
--
clamav (Emilio)
  NOTE: 20220510: Programming language C. (apo)
--
curl (Emilio)
  NOTE: 20220510: Programming language C.
--
debian-security-support (Utkarsh)
  NOTE: 20220402: need to update the list of unsupported packages (Beuc)
  NOTE: 20220402: check debian/README.source, sync with h01ger, and announce EOL'd packages (Beuc)
  NOTE: 20220402: context: https://lists.debian.org/debian-lts/2022/04/msg00000.html (Beuc)
  NOTE: 20220502: backport prepped, will contact Holger for more details. (utkarsh)
  NOTE: 20220516: in review, will also co-help Holger to maintain this. (utkarsh)
--
elog (Utkarsh)
  NOTE: 20220517: Please check further. It looks like a denial of service can be triggered remotely without
  NOTE: 20220517: authentication. If that is the case it should be fixed. If it cannot be triggered remotely then it can be postponed.
--
exempi
  NOTE: 20220517: A lot of packages reverse depends on libexmpi8. Further analysis
  NOTE: 20220517: is needed.
--
firmware-nonfree
  NOTE: 20210731: WIP: https://salsa.debian.org/lts-team/packages/firmware-nonfree
  NOTE: 20210828: Most CVEs are difficult to backport. Contacted Ben regarding possible "ignore" tag
  NOTE: 20211207: Intend to release this week.
--
gerbv
  NOTE: 20220321: WIP https://salsa.debian.org/lts-team/packages/gerbv (Anton)
  NOTE: 20220326: CVE-2021-40401 is fixed https://salsa.debian.org/lts-team/packages/gerbv/-/blob/debian/stretch/debian/patches/CVE-2021-40401.patch (Anton)
  NOTE: 20220326: CVE-2021-4040{0,2,3} do not have confirmed upstream fixes yet. (Anton)
--
golang-go.crypto
  NOTE: 20220331: rebuild reverse-dependencies if needed, e.g. DLA-2402-1 -> DLA-2453-1/DLA-2454-1/DLA-2455-1; also check buster status (Beuc)
--
gpac
  NOTE: 20211101: coordinating with secteam for s-p-u since stretch/buster versions match (roberto)
  NOTE: 20211120: received OK from secteam for buster update, working on stretch/buster in parallel (roberto)
  NOTE: 20211228: Returning to active work on this now that llvm/rustc update is complete (roberto)
  NOTE: 20220305: There are many dozens of open CVEs, it will take a while yet (roberto)
  NOTE: 20220413: New CVEs continue flooding in (roberto)
  NOTE: 20220427: Preparing to work with security team to declare EOL (roberto)
--
icingaweb2
  NOTE: https://people.debian.org/~abhijith/upload/mruby/icingaweb2_2.4.1-1+deb9u2.dsc (abhijith)
--
intel-microcode (Stefano Rivera)
  NOTE: 20220213: please recheck
--
kvmtool
  NOTE: 20220402: stretch-specific, orphaned package (Beuc)
  NOTE: 20220402: CVE-2021-45464 looks critical, check with upstream for acknowledgments/fixes (Beuc)
--
liblouis
  NOTE: 20220320: no patch available yet. Reproducible memory leaks with ASAN
  NOTE: 20220320: and POC. Consider fixing CVE-2018-17294 too.
  NOTE: 20220503: CVE-2022-26981 patch applied in salsa lts-team repo,
  NOTE: 20220503: Patch not applied upstream yet.
--
libpgjava
--
libvirt (Thorsten Alteholz)
  NOTE: 20220508: testing package
--
linux (Ben Hutchings)
--
linux-4.19 (Ben Hutchings)
--
mariadb-10.1
  NOTE: 20220222: Can be risky. Please consider backporting mariadb-10.3. See discussion https://lists.debian.org/debian-lts/2022/02/msg00005.html and coordinate with maintainer (Anton)
--
mbedtls (Utkarsh)
  NOTE: 20220404: update prepared, needs testing. (utkarsh)
  NOTE: 20220419: waiting for a quick feedback from carnil. (utkarsh)
  NOTE: 20220502: will upload with 1 fix and mark the other one
  NOTE: 20220502: as no-dsa today/tomorrow. (utkarsh)
  NOTE: 20220516: helf off upload to see if the other one should
  NOTE: 20220516: be squeezed in. waiting on -pu. (utkarsh)
--
mysql-connector-java
  NOTE: 20220512: Requires a new upstream version. (apo)
--
ntfs-3g
  NOTE: 20220515: Please recheck. There are currently not enough information
  NOTE: available. (apo)
--
nvidia-cuda-toolkit
   NOTE: 20220331: package is in non-free but also in packages-to-support (Beuc)
--
nvidia-graphics-drivers
  NOTE: 20220203: package is in non-free but also in packages-to-support (Beuc)
  NOTE: 20220209: monitor nvidia-graphics-drivers-legacy-390xx for a potential
  NOTE: 20220209: backport (apo)
--
openldap (Dominik George)
  NOTE: 20220512: Programming language C. back-sql is enabled but an experimental
  NOTE: 20220512: feature. (apo)
--
pdns
  NOTE: 20220402: harmonize with buster/10.8 (Beuc)
  NOTE: 20220506: buster patches backported in https://salsa.debian.org/enrico/pdns/-/tree/stretch
  NOTE: 20220506: and #debian-dns notified (enrico)
  NOTE: 20220506: the patch for https://security-tracker.debian.org/tracker/CVE-2022-27227
  NOTE: 20220506: would need to be completely rewritten for the stretch codebase (enrico)
  NOTE: 20220506: package builds but does not run a test suite, and I lack the
  NOTE: 20220506: know-how for testing manually (enrico)
--
puma
--
puppet-module-puppetlabs-firewall
  NOTE: 20220402: no Debian maintainers activity since 2018 (Beuc)
--
redis
  NOTE: 20220510: Chris Lamb is the maintainer. Programming language C. (apo)
--
ring (Abhijith PA)
  NOTE: 20220314: https://people.debian.org/~abhijith/upload/vda/ring_20161221.2.7bd7d91~dfsg1-1+deb9u2.dsc
  NOTE: 20220404: package in archive is faulty. New regs can't be done due (abhijith)
  NOTE: 20220404: a network error (abhijith)
  NOTE: 20220506: Pinged maintainer team and maintainer (abhijith)
--
ruby-devise-two-factor
  NOTE: 20220427: Patch does not apply cleanly to LTS version, may be due to this being the result
  NOTE: 20220427: of an incomplete fix to CVE-2015-7225. Will require some investigation. (lamby)
  NOTE: 20220502: should be marked as no-dsa; will send more details on the list. (utkarsh)
--
rsyslog (Sylvain Beucler)
  NOTE: 20220515: Please recheck. Vulnerable function processDataRcvd does not
  NOTE: seem to be affected but I'm not completely sure. Programming language C. (apo)
--
salt
--
samba
  NOTE: 20211128: WIP https://salsa.debian.org/lts-team/packages/samba/
  NOTE: 20211212: Fix is too large, coordination with ELTS-upload (anton)
  NOTE: 20220110: fix applied, but will need a second opinion. (utkarsh)
  NOTE: 20220125: ftbfs, wip. (utkarsh)
--
slurm-llnl
  NOTE: 20220516: Checking the code it looks like the patches will apply so the code is clearly vulnerable.
--
snapd
  NOTE: 20220308: seems vulnerable at least to setup_private_mount,
  NOTE: 20220308: but double check (pochu)
--
sox
  NOTE: 20220326: CVE-2019-13590 is fixed in git (Anton)
  NOTE: 20220326: https://salsa.debian.org/lts-team/packages/sox
  NOTE: 20220326: fix for CVE-2021-40426 is not yet available (Anton)
--
subversion
  NOTE: 20220422: Upstream's patch for CVE-2021-28544 does not cleanly apply (eg. "copyfrom_path = apr_pstrdup(...)" assignment)
  NOTE: 20220422: and, once applied manually, appears to break multiple and possibly unrelated parts of the testsuite. (lamby)
  NOTE: 20220501: Done some analysis, worked on a patch, cannot find a way to test it, mailed results to Roberto C. Sánchez (enrico)
--
thunderbird (Emilio)
--
tiff (Utkarsh)
  NOTE: 20220404: jessie upload at https://salsa.debian.org/lts-team/packages/tiff.
  NOTE: 20220404: if that works out well, I'll roll the same for stretch. (utkarsh)
  NOTE: 20220419: new CVE reported; waiting to see if there are more. (utkarsh)
  NOTE: 20220502: will collate the new CVEs and update the package. (utkarsh)
  NOTE: 20220513: more CVEs, ugh. Probably will consider rolling out the ones
  NOTE: 20220513: that are already applied and tested and re-add tiff here. (utkarsh)
--
unzip
  NOTE: 20220319: no patches yet but reproducible (apo)
  NOTE: 20220429: CVE-2022-0530: reported #1010355 with a proposed patch (enrico)
  NOTE: 20220429: CVE-2022-0529: sent a proposed patch to sanvila and team@s.d.o (enrico)
--
