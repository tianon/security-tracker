CVE-2021-43861
	[bullseye] - node-mermaid 8.7.0+ds+~cs27.17.17-3+deb11u1
CVE-2021-44906
	[bullseye] - node-minimist 1.2.5+~cs5.3.1-2+deb11u1
CVE-2022-24773
	[bullseye] - node-node-forge 0.10.0~dfsg-3+deb11u1
CVE-2022-24772
	[bullseye] - node-node-forge 0.10.0~dfsg-3+deb11u1
CVE-2022-24771
	[bullseye] - node-node-forge 0.10.0~dfsg-3+deb11u1
CVE-2022-0686
	[bullseye] - node-url-parse 1.5.3-1+deb11u1
CVE-2022-0691
	[bullseye] - node-url-parse 1.5.3-1+deb11u1
CVE-2021-3654
	[bullseye] - nova 2:22.2.2-1+deb11u1
CVE-2021-40083
	[bullseye] - knot-resolver 5.3.1-1+deb11u1
CVE-2021-32718
	[bullseye] - rabbitmq-server 3.8.9-3+deb11u1
CVE-2021-32719
	[bullseye] - rabbitmq-server 3.8.9-3+deb11u1
CVE-2021-22116
	[bullseye] - rabbitmq-server 3.8.9-3+deb11u1
CVE-2018-1279
	[bullseye] - rabbitmq-server 3.8.9-3+deb11u1
CVE-2022-21814
	[bullseye] - nvidia-graphics-drivers 470.103.01-1~deb11u1
CVE-2022-21813
	[bullseye] - nvidia-graphics-drivers 470.103.01-1~deb11u1
CVE-2021-39191
	[bullseye] - libapache2-mod-auth-openidc 2.4.9.4-1+deb11u1
CVE-2020-7711
	[bullseye] - golang-github-russellhaering-goxmldsig 1.1.0-1+deb11u1
CVE-2022-25308
	[bullseye] - fribidi 1.0.8-2+deb11u1
CVE-2022-25309
	[bullseye] - fribidi 1.0.8-2+deb11u1
CVE-2022-25310
	[bullseye] - fribidi 1.0.8-2+deb11u1
CVE-2022-26505
	[bullseye] - minidlna 1.3.0+dfsg-2+deb11u1
CVE-2022-24785
	[bullseye] - node-moment 2.29.1+ds-2+deb11u1
CVE-2021-43566
	[bullseye] - samba 2:4.13.13+dfsg-1~deb11u4
CVE-2022-1328
	[bullseye] - mutt 2.0.5-4.1+deb11u1
CVE-2022-0436
	[bullseye] - grunt 1.3.0-1+deb11u1
CVE-2022-27406
	[bullseye] - freetype 2.10.4+dfsg-1+deb11u1
CVE-2022-27405
	[bullseye] - freetype 2.10.4+dfsg-1+deb11u1
CVE-2022-27404
	[bullseye] - freetype 2.10.4+dfsg-1+deb11u1
CVE-2022-29078
	[bullseye] - node-ejs 2.5.7-3+deb11u1
CVE-2022-21227
	[bullseye] - node-sqlite3 5.0.0+ds1-1+deb11u1
CVE-2022-24851
	[bullseye] - ldap-account-manager 7.4-1+deb11u1
CVE-2022-24801
	[bullseye] - twisted 20.3.0-7+deb11u1
CVE-2022-21716
	[bullseye] - twisted 20.3.0-7+deb11u1
CVE-2022-21712
	[bullseye] - twisted 20.3.0-7+deb11u1
CVE-2022-27240
	[bullseye] - glewlwyd 2.5.2-2+deb11u3
CVE-2022-29967
	[bullseye] - glewlwyd 2.5.2-2+deb11u3
CVE-2022-30333
	[bullseye] - unrar-nonfree 1:6.0.3-1+deb11u1
CVE-2022-1650
	[bullseye] - node-eventsource 1.0.7-1+deb11u1
CVE-2021-3618
	[bullseye] - nginx 1.18.0-6.1+deb11u2
