CVE-2021-44906
	[buster] - node-minimist 1.2.0-1+deb10u2
CVE-2022-24773
	[buster] - node-node-forge 0.8.1~dfsg-1+deb10u1
CVE-2022-24772
	[buster] - node-node-forge 0.8.1~dfsg-1+deb10u1
CVE-2022-24771
	[buster] - node-node-forge 0.8.1~dfsg-1+deb10u1
CVE-2019-20446
	[buster] - librsvg 2.44.10-2.1+deb10u1
CVE-2019-17134
	[buster] - octavia 3.0.0-3+deb10u1
CVE-2019-14433
	[buster] - nova 2:18.1.0-6+deb10u1
CVE-2019-14857
	[buster] - libapache2-mod-auth-openidc 2.3.10.2-1+deb10u1
CVE-2020-8492
	[buster] - python2.7 2.7.16-2+deb10u2
CVE-2019-20907
	[buster] - python2.7 2.7.16-2+deb10u2
CVE-2021-3177
	[buster] - python2.7 2.7.16-2+deb10u2
CVE-2020-24583
	[buster] - python-django 1:1.11.29-1~deb10u2
CVE-2020-24584
	[buster] - python-django 1:1.11.29-1~deb10u2
CVE-2021-3281
	[buster] - python-django 1:1.11.29-1~deb10u2
CVE-2021-23336
	[buster] - python-django 1:1.11.29-1~deb10u2
CVE-2020-4051
	[buster] - dojo 1.14.2+dfsg1-1+deb10u3
CVE-2021-32062
	[buster] - mapserver 7.2.2-1+deb10u1
CVE-2020-35572
	[buster] - adminer 4.7.1-1+deb10u1
CVE-2021-21311
	[buster] - adminer 4.7.1-1+deb10u1
CVE-2021-29625
	[buster] - adminer 4.7.1-1+deb10u1
CVE-2021-35525
	[buster] - postsrsd 1.5-2+deb10u2
CVE-2021-3801
	[buster] - node-prismjs 1.11.0+dfsg-3+deb10u1
CVE-2021-3930
	[buster] - qemu 1:3.1+dfsg-8+deb10u9
CVE-2021-3748
	[buster] - qemu 1:3.1+dfsg-8+deb10u9
CVE-2021-3713
	[buster] - qemu 1:3.1+dfsg-8+deb10u9
CVE-2021-3682
	[buster] - qemu 1:3.1+dfsg-8+deb10u9
CVE-2021-3608
	[buster] - qemu 1:3.1+dfsg-8+deb10u9
CVE-2021-3607
	[buster] - qemu 1:3.1+dfsg-8+deb10u9
CVE-2021-3582
	[buster] - qemu 1:3.1+dfsg-8+deb10u9
CVE-2021-3527
	[buster] - qemu 1:3.1+dfsg-8+deb10u9
CVE-2021-3392
	[buster] - qemu 1:3.1+dfsg-8+deb10u9
CVE-2021-20257
	[buster] - qemu 1:3.1+dfsg-8+deb10u9
CVE-2021-20221
	[buster] - qemu 1:3.1+dfsg-8+deb10u9
CVE-2021-20203
	[buster] - qemu 1:3.1+dfsg-8+deb10u9
CVE-2021-20196
	[buster] - qemu 1:3.1+dfsg-8+deb10u9
CVE-2021-20181
	[buster] - qemu 1:3.1+dfsg-8+deb10u9
CVE-2020-35505
	[buster] - qemu 1:3.1+dfsg-8+deb10u9
CVE-2020-35504
	[buster] - qemu 1:3.1+dfsg-8+deb10u9
CVE-2020-27617
	[buster] - qemu 1:3.1+dfsg-8+deb10u9
CVE-2020-25723
	[buster] - qemu 1:3.1+dfsg-8+deb10u9
CVE-2020-25624
	[buster] - qemu 1:3.1+dfsg-8+deb10u9
CVE-2020-25625
	[buster] - qemu 1:3.1+dfsg-8+deb10u9
CVE-2020-25085
	[buster] - qemu 1:3.1+dfsg-8+deb10u9
CVE-2020-25084
	[buster] - qemu 1:3.1+dfsg-8+deb10u9
CVE-2020-15859
	[buster] - qemu 1:3.1+dfsg-8+deb10u9
CVE-2020-13253
	[buster] - qemu 1:3.1+dfsg-8+deb10u9
CVE-2015-9541
	[buster] - qtbase-opensource-src 5.11.3+dfsg1-1+deb10u5
CVE-2020-7711
	[buster] - golang-github-russellhaering-goxmldsig 0.0~git20170911.b7efc62-1+deb10u1
CVE-2022-25308
	[buster] - fribidi 1.0.5-3.1+deb10u2
CVE-2022-25309
	[buster] - fribidi 1.0.5-3.1+deb10u2
CVE-2022-25310
	[buster] - fribidi 1.0.5-3.1+deb10u2
CVE-2022-26505
	[buster] - minidlna 1.2.1+dfsg-2+deb10u3
CVE-2019-12953
	[buster] - dropbear 2018.76-5+deb10u1
CVE-2022-1328
	[buster] - mutt 1.10.1-2.1+deb10u6
CVE-2022-27406
	[buster] - freetype 2.9.1-3+deb10u3
CVE-2022-27405
	[buster] - freetype 2.9.1-3+deb10u3
CVE-2022-27404
	[buster] - freetype 2.9.1-3+deb10u3
CVE-2021-0561
	[buster] - flac 1.3.2-3+deb10u2
CVE-2022-29078
	[buster] - node-ejs 2.5.7-1+deb10u1
CVE-2019-12387
	[buster] - twisted 18.9.0-3+deb10u1
CVE-2019-12855
	[buster] - twisted 18.9.0-3+deb10u1
CVE-2019-9511
	[buster] - twisted 18.9.0-3+deb10u1
CVE-2019-9514
	[buster] - twisted 18.9.0-3+deb10u1
CVE-2019-9515
	[buster] - twisted 18.9.0-3+deb10u1
CVE-2020-10108
	[buster] - twisted 18.9.0-3+deb10u1
CVE-2020-10109
	[buster] - twisted 18.9.0-3+deb10u1
CVE-2022-21712
	[buster] - twisted 18.9.0-3+deb10u1
CVE-2022-21716
	[buster] - twisted 18.9.0-3+deb10u1
CVE-2022-24801
	[buster] - twisted 18.9.0-3+deb10u1
CVE-2022-3033
	[buster] - unrar-nonfree 1:5.6.6-1+deb10u1
